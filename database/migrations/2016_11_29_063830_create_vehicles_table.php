<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_vehicles_categories', function (Blueprint $table) {
            
        });
        
        Schema::create('t_vehicles', function (Blueprint $table) {
            // EXT_ID	ECV	MODEL	MANUFACTURER	CATEGORY	CODE	CENTER	STATE	RP_LOGIN	RP_JOB	VALID	MOID	ID	LOCALITY	KOD_LOKALITY	T_LOCALITY_C	DVCO	VEH_CAT	POOL
            $table->increments('id')->unsigned()->index();
            $table->string('moid', 10)->comment('id meracej GPS jednotky');
            $table->string('ecv', 10)->comment('evidenčné číslo vozidla');
            $table->string('manufacturer', 20)->comment('výrobca vozidla (Škoda)')->nullable();
            $table->string('model', 20)->comment('model vozidla (Fabia 1.4)')->nullable();

            $table->string('login', 20)->comment('login pracovníka zodpovedného za vozidlo');
            $table->string('job', 12)->comment('PM zodpovedného pracovníka');
            $table->string('valid', 1)->comment('určuje, či je platný záznam')->default('Y');

			$table->integer('veh_cat_id')->comment('kategória vozidla, DVCO')->unsigned()->index();
			$table->foreign('veh_cat_id')->references('id')->on('t_vehicle_categories')->onDelete('cascade'); 

            $table->integer('center_id')->comment('stredisko dopravy, lokalita, ktorému vozidlo prináleží');
            $table->foreign('center_id')->references('id')->on('t_vehicle_categories')->onDelete('cascade'); 
            //$table->decimal('price', 6, 2)->comment('cena');;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicles');
    }
}
